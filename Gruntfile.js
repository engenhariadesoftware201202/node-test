var nodemon = require('./gruntTasks/nodemon');
var copy = require('./gruntTasks/copy');
var concat = require('./gruntTasks/concat');
var watch = require('./gruntTasks/watch');
var concurrent = require('./gruntTasks/concurrent');
var mochaTest = require('./gruntTasks/mocha');

module.exports = function(grunt) {
    grunt.config.merge({
        globalConfig: {
            deployPath: 'server/public'
        }
    });
    grunt.config.merge(copy);
    grunt.config.merge(concat);
    grunt.config.merge(nodemon);
    grunt.config.merge(watch);
    grunt.config.merge(concurrent);
    grunt.config.merge(mochaTest);

    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-concurrent');

    grunt.registerTask('default', ['copy', 'concat', 'concurrent:develop']);
    grunt.registerTask('compileWebSite', ['copy', 'concat']);
    grunt.registerTask('server', ['nodemon']);
    grunt.registerTask('test', ['mochaTest']);
};
