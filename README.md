Softruck Technical Test
=======================

  This code crawls over [ANP Web Site](http://www.anp.gov.br/preco/index.asp) options:
  
  * "Por Município";
  * "Por Estado".
  
  It shows the prices from different types of fuels around the country.

Installation
------------
`npm install`

  This will install all the dependencies.
  
  Please, remember that you need to have [NodeJS](https://nodejs.org/), [MondoDB](https://www.mongodb.org/),
  [Grunt-CLI](https://github.com/gruntjs/grunt-cli) already installed in your machine before executing these
  command.

###Dependencies

  Backend Dependencies:
  * [NodeJS](https://nodejs.org/)
  * [Express](https://github.com/strongloop/express)
  * [body-parser](https://github.com/expressjs/body-parser)
  * [MondoDB](https://www.mongodb.org/)
  * [Mongoose](http://mongoosejs.com/)
  * [request](https://github.com/request/request)
  * [cheerio](https://github.com/cheeriojs/cheerio)
  
  Frontend Dependencies:
  * [angular](https://angularjs.org/)
  * [angular-route](https://www.npmjs.com/package/angular-route)
  * [angular-material](https://material.angularjs.org/latest/#/)
    * [angular-animate](https://www.npmjs.com/package/angular-animate)
    * [angular-aria](https://libraries.io/npm/angular-aria)
    * [angular-messages](https://libraries.io/npm/angular-messages)
  
  Developing Dependencies:
  * [grunt](http://gruntjs.com/)
  * [grunt-cli](https://github.com/gruntjs/grunt-cli) (Must be global)
  * [grunt-contrib-copy](https://github.com/gruntjs/grunt-contrib-copy)
  * [grunt-contrib-concat](https://github.com/gruntjs/grunt-contrib-concat)
  * [grunt-contrib-watch](https://github.com/gruntjs/grunt-contrib-watch)
  * [grunt-concurrent](https://github.com/sindresorhus/grunt-concurrent)
  * [grunt-mocha-test](https://github.com/pghalliday/grunt-mocha-test)
  * [grunt-nodemon](https://github.com/ChrisWren/grunt-nodemon)
  * [mocha](https://www.npmjs.com/package/mocha)
  * [should](https://www.npmjs.com/package/should)
  
Grunt Tasks
----------

  To run the server you just have to execute:
  `$ grunt`
  
  This will activate the developing mode, any changes in the code will trigger all the deploy pipe line for each end.
  
  To compile th web site you have to execute:
  `$ grunt compileWebSite`
  Remember that in `Gruntfile.js` there is a variable *deployPath* that defines where grunt will deploy te web site.
  
  To run just the server with its contents unmodified (Web Site will not compile):
  `$ grunt server`
  
  To run Unit test you have to execute:
  `$ grunt test`

Interacting With Web Site
-------------------------

  After you put the server up, go to [localhost](http://localhost:3000/#) and choose the state that you would like to
  lookup.
  
  You can change the `port` and `url` of you server changes the config file at:
  `server/config.js`

Endpoints Available
-------------------

  * '/' - Redirects to the site;
  * '/search/parameters/' - GET - Get all the states that ANP has available (and other necessary data)
  * '/gas/prices' - POST - Returns all the that collected on the ANP web site.
  
Improvements
------------

   * Model Week was created to be easier to track which weeks we already searched for. This way he could build a History
     page.
   * Improve how we are retrieving data from ANP site. I though about breaking the data into chunks (as you can see there
     are more models) save it into the database and form the response JSON using the Mongo search engine.
   * Angular-Material still to fresh, some components do not work as expected (there is no way to use tabs inside tabs)
   * Server still not robust enough
   * Get better error messages
   * Create more useful logs
   * Implement [winston](https://github.com/winstonjs/winston) as a server logger
  
Tests
-----
  
  Testing mongoose or MongoDB is out of scope of this project, this is why I didn't test any files in models and persistence.
  Those files do not have any important business/logic code.
  
  Files tested:
  
  * ModelHelpers
  * gasPriceController
  * searchParametersController
  
  OBS: couldn't finish up tests for searchParametersController. There is no coverage task because I couldn't finish it in time.