(function(){
    angular.module('GasCrawlerApp', ['ngMaterial', 'ngRoute', 'ngMessages']).config(function($mdThemingProvider){
        $mdThemingProvider.theme('default')
            .primaryPalette('blue')
            .accentPalette('orange');
    });
})();
