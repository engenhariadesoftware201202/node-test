(function(){

    angular.module('GasCrawlerApp').filter('dateTranslate', function() {
        return function(input) {
          return input.replace('de', 'from').replace('a', 'to');
        };
    });

})();