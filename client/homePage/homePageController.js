(function(){

    angular.module('GasCrawlerApp').controller('homePageController', function($scope, $http){
        $scope.states = [];
        $scope.parameters = {
            selEstado: '',
            selCombustivel: '',
            desc_Semana: ''
        };
        $scope.cities = [];
        $scope.loadingResults = false;
        $scope.loadingParameters = true;

        $scope.search = function() {
            $scope.loadingResults = true;
            $http.post('/gas/prices', $scope.parameters).success(function(prices) {
                $scope.state = prices;
                $scope.loadingResults = false;
            });
        };

        $scope.openStatisticList = function() {
            $scope.showStatistics = !$scope.showStatistics;
        };

        $scope.openStationList = function() {
            $scope.showStatistics = false;
            $scope.showStations = !$scope.showStations;
        };

        $scope.toggleChildData = function(item) {
            item.opened = !item.opened;
        };

        $scope.toggleCityStatistic = function(item) {
            item.statisticOpened = !item.statisticOpened;
        };

        $scope.toggleCityStations = function(item) {
            item.stationOpened = !item.stationOpened;
        };


        $http.get('/search/parameters').success(function(parameters){
            $scope.parameters = parameters;
            //copying arrays, I will not need this once I've loaded it into the page
            $scope.states = parameters.states.slice();
            delete parameters.states;
            $scope.loadingParameters = false;
        });
    });

})();
