(function() {

    angular.module('GasCrawlerApp').config(function($routeProvider){
        $routeProvider
            .when('/', {
                controller: 'homePageController',
                templateUrl: 'views/homePageView.html'
            })
            .otherwise({ redirectTo: '/' });
    });

})();
