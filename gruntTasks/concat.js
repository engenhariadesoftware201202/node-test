module.exports = {
    concat: {
        appJS: {
            src: ['client/**/*.js'],
            dest: '<%= globalConfig.deployPath  %>/app.js'
        },
        appCSS: {
            src: ['client/**/*.css'],
            dest: '<%= globalConfig.deployPath  %>/app.css'
        }
    }
};