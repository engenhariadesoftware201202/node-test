module.exports = {
    concurrent: {
        develop: {
            tasks : ['nodemon', 'watch'],
            options: {
                logConcurrentOutput: true
            }
        }
    }
};