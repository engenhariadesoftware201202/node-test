module.exports = {
    copy: {
        javascripts: {
            files: [
                {
                    expand: true,
                    flatten: true,
                    src: ['node_modules/angular/*.min.js', 'node_modules/angular/*.map'],
                    dest: '<%= globalConfig.deployPath  %>/lib/js'
                },
                {
                    expand: true,
                    flatten: true,
                    src: ['node_modules/angular-animate/*.min.js', 'node_modules/angular-animate/*.map'],
                    dest: '<%= globalConfig.deployPath  %>/lib/js'
                },
                {
                    expand: true,
                    flatten: true,
                    src: ['node_modules/angular-messages/*.min.js', 'node_modules/angular-messages/*.map'],
                    dest: '<%= globalConfig.deployPath  %>/lib/js'
                },
                {
                    expand: true,
                    flatten: true,
                    src: ['node_modules/angular-route/*.min.js', 'node_modules/angular-route/*.map'],
                    dest: '<%= globalConfig.deployPath  %>/lib/js'
                },
                {
                    expand: true,
                    flatten: true,
                    src: ['node_modules/angular-aria/*.min.js', 'node_modules/angular-aria/*.map'],
                    dest: '<%= globalConfig.deployPath  %>/lib/js'
                },
                {
                    expand: true,
                    flatten: true,
                    src: ['node_modules/angular-material/*.min.js'],
                    dest: '<%= globalConfig.deployPath  %>/lib/js'
                }
            ]
        },
        styles: {
            files: [
                {
                    expand: true,
                    flatten: true,
                    src: ['node_modules/angular-material/*.min.css'],
                    dest: '<%= globalConfig.deployPath  %>/lib/css'
                }
            ]
        },
        directives: {
            files: [
                {
                    expand: true,
                    flatten: true,
                    src: ['client/**/*Directive.html'],
                    dest: '<%= globalConfig.deployPath  %>/directives'
                }
            ]
        },
        views: {
            files: [
                {
                    expand: true,
                    flatten: true,
                    src: ['client/**/*View.html'],
                    dest: '<%= globalConfig.deployPath  %>/views'
                }
            ]
        },
        index: {
            files: [
                {
                    expanded: true,
                    flatten: true,
                    src: ['client/index.html'],
                    dest: '<%= globalConfig.deployPath  %>/index.html'
                }
            ]
        },
        favicon: {
            files: [
                {
                    expanded: true,
                    flatten: true,
                    src: ['client/favicon.ico'],
                    dest: '<%= globalConfig.deployPath  %>/favicon.ico'
                }
            ]
        }
    }
};