module.exports = {
    mochaTest: {
        test: {
            options: {
                reporter: 'spec'
            },
            src: ['tests/**/*.js']
        }
    }
};