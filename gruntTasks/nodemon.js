module.exports = {
    nodemon: {
        express: {
            script: 'index.js',
            options: {
                watch: ['server/'],
                cwd: 'server'
            }
        }
    }
};