module.exports = {
    watch: {
        html: {
            files: ['client/**/*.html'],
            tasks: ['copy:views', 'copy:directives']
        },
        indexHtml: {
            files: ['client/index.html'],
            tasks: ['copy:index']
        },
        scripts: {
            files: ['client/**/*.js'],
            tasks: ['concat:appJS']
        },
        styles: {
            files: ['client/**/*.css'],
            tasks: ['concat:appCSS']
        }

    }
};