module.exports = {
    host: 'localhost',
    port: 3000,
    anp: {
        baseURL: 'http://www.anp.gov.br/preco/prc/',
        statesPath: 'Resumo_Por_Estado_Index.asp',
        valuesPath: 'Resumo_Por_Estado_Municipio.asp',
        stationPath: 'Resumo_Por_Municipio_Posto.asp',
        cityPath: 'Resumo_Por_Municipio_Index.asp'
    },
    timeout: 3000,
    mongo:{
        url: 'mongodb://localhost:27017/softruck',
        options: {
            db: {
                safe: true
            }
        }
    }
};
