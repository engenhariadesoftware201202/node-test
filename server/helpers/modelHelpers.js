module.exports = {
    /**
     * Just a converting string to number
     *
     * @param stringNumber {String} String to be converted
     * @returns {String) converted
     **/
    stringToNumber: function(stringNumber) {
        return Number(stringNumber.replace(',', '.'));
    },
    /**
     * Normalizing Strings. The data from ANP web site comes a little dirty. Here we remove '*' and '@' from
     * the values that we have
     *
     * @param string {String} string to be normalized
     * @returns {String} normalized string
     */
    normalizeString: function(string) {
        return string.split('*')[1].replace(/@/g, ' ');
    }
};
