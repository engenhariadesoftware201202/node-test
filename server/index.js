var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var gasPrices = require('./routes/gasPrices/gasPricesRouting');
var searchParameters = require('./routes/searchParameters/searchParametersRouting');
var config = require('./config');
var app = express();

console.log('Connecting to the database');
mongoose.connect(config.mongo.url, config.mongo.options);

console.log('Making public folder public');
app.use(express.static('./public'));

console.log('Adding Middlewares');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log('Adding route');
app.use('/gas/prices', gasPrices);
app.use('/search/parameters', searchParameters);

console.log('Initializing server');

var server = app.listen(config.port, config.host, function() {
  var address = server.address();

  console.log('Listening at http://%s:%s', address.address, address.port);
});