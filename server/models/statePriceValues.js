var mongoose = require('mongoose');
var ModelHelper = require('../helpers/modelHelpers');
var Schema = mongoose.Schema;

var StatePriceValuesSchema = new Schema({
    name:     {type: String, set: ModelHelper.normalizeString},
    weekCode: {type: Number, set: ModelHelper.stringToNumber},
    cities:   []
});

module.exports = mongoose.model('StatePriceValues', StatePriceValuesSchema);