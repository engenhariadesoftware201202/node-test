var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var StationSchema = new Schema({
    name:     String,
    address:  String,
    area:     String,
    flag:     String,
    prices:   []
});

module.exports = mongoose.model('Station', StationSchema);