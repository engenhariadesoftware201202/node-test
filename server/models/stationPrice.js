var mongoose = require('mongoose');
var ModelHelper = require('../helpers/modelHelpers');
var Schema = mongoose.Schema;

var StationPriceSchema = new Schema({
    type:       String,
    sellPrice:  {type: Number, set: ModelHelper.stringToNumber},
    buyPrice:   {type: Number, set: ModelHelper.stringToNumber},
    saleMode:   String,
    provider:   String,
    date:       String
});

module.exports = mongoose.model('StationPrice', StationPriceSchema);