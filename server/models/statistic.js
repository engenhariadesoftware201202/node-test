var mongoose = require('mongoose');
var ModelHelper = require('../helpers/modelHelpers');
var Schema = mongoose.Schema;

var StatisticSchema = new Schema({
    type:              {type: String, set: ModelHelper.normalizeString},
    state:             {type: String, set: ModelHelper.normalizeString},
    city:              String,
    weekCode:          {type: Number, set: ModelHelper.stringToNumber},
    consumerPrice:     [],
    distributionPrice: []

});

module.exports = mongoose.model('Statistic', StatisticSchema);