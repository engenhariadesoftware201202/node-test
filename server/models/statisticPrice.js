var mongoose = require('mongoose');
var ModelHelper = require('../helpers/modelHelpers');
var Schema = mongoose.Schema;

var StatisticPriceSchema = new Schema({
    averagePrice:       {type: Number, set: ModelHelper.stringToNumber},
    standardDeviation:  {type: Number, set: ModelHelper.stringToNumber},
    minPrice:           {type: Number, set: ModelHelper.stringToNumber},
    maxPrice:           {type: Number, set: ModelHelper.stringToNumber},
    averageMargin:      {type: Number, set: ModelHelper.stringToNumber}
});

module.exports = mongoose.model('StatisticPrice', StatisticPriceSchema);