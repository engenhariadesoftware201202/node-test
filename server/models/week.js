var mongoose = require('mongoose');
var ModelHelper = require('../helpers/modelHelpers');
var Schema = mongoose.Schema;

var WeekSchema = new Schema({
    description: String,
    weekCode:    {type: Number, set: ModelHelper.stringToNumber}
});

module.exports = mongoose.model('Week', WeekSchema);