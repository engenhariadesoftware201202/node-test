var StatePriceValues = require('../models/StatePriceValues');

var StatePriceValueDAO = function() {

    /**
     * Creates a StatePriceValues instance
     *
     * @param state {String} state name
     * @param weekCode {String} code that ANP uses to identify a week
     * @returns {StatePriceValues}
     */
    this.createStatePriceValues = function(state, weekCode) {
        var stePriceValues = new StatePriceValues({
            name: state,
            weekCode: weekCode
        });

        return stePriceValues;
    };

    /**
     *
     * Try to find the state values based on the week provided
     *
     * @param weekCode {Number} number that ANP uses to identify a week
     * @param stateName {String} state's name
     * @param successCB {Function} callback to be called if the query was a success
     * @param failCB {Function} callback to be called if the quart has failed
     * @returns {StatePriceValues}
     */
    this.findStatePricesByWeekCode = function(weekCode, stateName, successCB, failCB) {
        return StatePriceValues.findOne({weekCode: weekCode, name: stateName}, function(error, statePriceValues) {
            if(error || !statePriceValues) {
                console.log(error);
                failCB();
            } else {
                successCB(statePriceValues);
            }
        });
    };

    /**
     *
     * Saves StatePriceValues into database
     *
     * @param statePriceValues
     */
    this.saveStatePriceValues = function(statePriceValues) {
        statePriceValues.save(function(error, savedData) {
            if(error) {
                console.log('Could not save State Price Values!');
            } else {
                console.log('State Price Saved. #', savedData._id);
            }
        })
    }
};

module.exports = StatePriceValueDAO;

