var StatisticPrice = require('../models/statisticPrice');
var Statistic = require('../models/statistic');

var StatisticDAO = function() {

    /**
     *
     * Save Statistics into database
     *
     * @param statistic {Statistic} model to be saved
     */
    this.saveStatistic = function(statistic) {
        statistic.save(function(error) {
            if(!error) {
                console.log('Statistic saved');
            } else {
                console.log(error);
            }
        });
    };

    /**
     *
     * Finds the statistics for a states related to a week
     *
     * @param state {String} state's name
     * @param weekCode {Numbe} number used by ANP to identify a week
     * @param successCB {Function) function to be called when the query is successful
     */
    this.findStatisticsForState = function(state, weekCode, successCB) {
        Statistic.find({'weekCode': weekCode, 'state': state}, function(error, statistics){
            if(!error) {
                console.log(statistics);
            } else {
                successCB(statistics);
            }
        });
    };

    /**
     *
     * Creates the Statistics
     *
     * @param statisticsValue {Array} values retrieved from ANP site in an array
     * @returns {Statistics}
     */
    this.createStatistic = function(statisticsValue) {
        if(statisticsValue.length === 11) {
            var statistic = {
                city: statisticsValue[0],
                consumerPrice: [],
                distributionPrice: []
            };

            var consumerStatisticPrice = new StatisticPrice({
                averagePrice: statisticsValue[2],
                standardDeviation: statisticsValue[3],
                minPrice: statisticsValue[4],
                maxPrice: statisticsValue[5],
                averageMargin: statisticsValue[6]
            });

            var distributionStatisticPrice = new StatisticPrice({
                averagePrice: statisticsValue[7],
                standardDeviation: statisticsValue[8],
                minPrice: statisticsValue[9],
                maxPrice: statisticsValue[10],
                averageMargin: 0
            });

            statistic.consumerPrice.push(consumerStatisticPrice);
            statistic.distributionPrice.push(distributionStatisticPrice);

            return statistic;
        } else {
            console.log('There was an error creating statistics.', statisticsValue);
            return {};
        }
    }

};

module.exports = StatisticDAO;