var StationPrice = require('../models/stationPrice');
var Station = require('../models/station');

var StationDAO = function() {

    /**
     *
     * Save Station to the database
     *
     * @param stations
     */
    this.saveStations = function(stations) {
        stations.save(function(error) {
            if(!error) {
                console.log('Stations saved');
            } else {
                console.log(error);
            }
        });
    };

    /**
     *
     * Find in database a station related to this state
     *
     * @param state {String} state's name
     * @param weekCode {Number} number that ANP uses to identify a week
     * @param successCB {Function} function to be called when the query is successful
     */
    this.findStationForState = function(state, weekCode, successCB) {
        console.log('Finding data with: ', state, weekCode);
        Station.find({'weekCode': weekCode, 'state': state}, function(error, statistics){
            if(!error) {
                console.log(statistics);
            } else {
                successCB(statistics)
            }
        });
    };

    /**
     *
     * Creates a Station.
     *
     * @param stationValues {Array} values retrieved from ANP site in an array
     * @param fuelType {String} fuel type
     * @param city {String} city's name from where this stations is located
     * @returns {Station}
     */
    this.createStation = function(stationValues, fuelType, city) {

        var data = {
            city: city
        };

        var station = new Station({
            name:     stationValues['raz�o social'] || '',
            address:  stationValues['endere�o'] || '',
            area:     stationValues['bairro'] || '',
            flag:     stationValues['bandeira'] || '',
            prices:   []
        });

        var stationPrice = new StationPrice({
            type:       fuelType,
            sellPrice:  stationValues['pre�o venda'] || '',
            buyPrice:   stationValues['pre�o compra'] || '',
            saleMode:   stationValues['modalidade de compra'] || '',
            provider:   stationValues['fornecedor (b. branca)'] || '',
            date:       stationValues['data coleta'] || ''
        });

        station.prices.push(stationPrice);
        data.station = station;

        return data;

    };
};

module.exports = StationDAO;

