var Week = require('../models/week');

var WeekDAO = function() {

    /**
     *
     * Creates a Week
     *
     * @param weekCode {String} number that ANP uses to identify a week
     * @param description {String} sentence that says the dates that define this week
     * @returns {Week}
     */
    this.createWeek = function(weekCode, description) {
        var week = new Week({
            description: description,
            weekCode:    weekCode
        });

        return week;
    };

    /**
     *
     * Saes Week into database
     *
     * @param week {Week} model to be saved
     */
    this.saveWeek = function(week) {
        week.save(function(error, savedData) {
            if(error) {
                console.log('Could not save Week!. ', error);
            } else {
                console.log('Week Saved. #', savedData._id);
            }
        })
    }
};

module.exports = WeekDAO;

