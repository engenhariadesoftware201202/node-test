var req = require('request');
var cheerio = require('cheerio');
var StatisticDAO = require('../../persistence/StatisticDAO');
var StationDAO = require('../../persistence/StationDAO');
var StatePriceValueDAO = require('../../persistence/StatePriceValueDAO');
var ModelHelper = require('../../helpers/modelHelpers');
var config = require('../../config');


var gasPricesController = function () {

    var waiting = 0;
    var statisticsDAO = new StatisticDAO();
    var me = this;

    /**
     *
     * Format the data to be sent to the requester. I did this because I needed to map all data that I retrieved from
     * ANP site, however, doing so lead me to a JSON which differs from what was asked, hence the formatting
     *
     * @param oldCities {Object} data collected from ANP web site
     * @returns {Array} cities formatted correctly
     */
    this.formatCitiesFromData = function(oldCities) {
        var cities = [];

        for (var cityValues in oldCities) {
            if (oldCities.hasOwnProperty(cityValues)) {
                var city = {
                    name: cityValues,
                    stations:[],
                    statistics: oldCities[cityValues].statistics //statistics is correctly formatted
                };

                var oldStations = oldCities[cityValues].stations;

                for(var station in oldStations) {
                    if(oldStations.hasOwnProperty(station)){
                        city.stations.push(oldStations[station]);
                    }
                }

                cities.push(city);
            }
        }

        return cities;
    };

    /**
     *
     * Decides if it is time to send the data to the requester. Since all the requests are asynchronous this was
     * necessary to know when the data would be ready.
     *
     * @param response {Object} express's response object
     * @param cities {Object} data from ANP web site unformatted
     * @param stateResponse {Object} data to be sent to the requester
     */
    var sendRequest = function(response, cities, stateResponse) {
        if(--waiting === 0) {
            var statePriceValuesDAO = new StatePriceValueDAO();

            stateResponse.cities = me.formatCitiesFromData(cities);
            statePriceValuesDAO.saveStatePriceValues(stateResponse);

            console.log('Enviando data');
            response.send(stateResponse);
        }
    };

    /**
     *
     * Adds statistics information to the unformatted data. This is where I keep track of the data that I already got.
     *
     * @param data {Object}  data to be added
     * @param cities {Object} unformatted data
     * @param fuelName {String} fuel's name related to this data
     */
    this.addStatisticsToCities = function(data, cities, fuelName) {
        if(!cities[data.city]) {
            cities[data.city] = {};
            cities[data.city].statistics = [];
            cities[data.city].stations = {};
        }

        data.type = fuelName;

        cities[data.city].statistics.push(data);
    };

    /**
     *
     * Adds station information to the unformatted data. This is where I keep track of the data that I already got.
     *
     * @param data {Object} data to be added
     * @param cities {Object} unformatted data
     */
    var addStationToCities = function(data, cities) {
        if(!cities[data.city]) {
            cities[data.city] = {};
            cities[data.city].statistics = [];
            cities[data.city].stations = {};
        }

        if(cities[data.city].stations[data.station.name+'-'+data.station.area]) {
            cities[data.city].stations[data.station.name+'-'+data.station.area].prices.push(data.station.prices[0]);
        } else {
            cities[data.city].stations[data.station.name+'-'+data.station.area] = data.station;
        }
    };

    /**
     *
     * Search through the response for usable data. Station's data respectively.
     *
     * @param body {String} string with the web site's html
     * @param selectorPrefix {String} prefix to determine which element to get
     * @returns {Object} It contains an Array of Arrays, which each item represents a line in the table
     */
    var dataFormatter2 = function(body, selectorPrefix) {
        var $ = cheerio.load(body);
        var values = {};
        var objects = [];
        var selectorSuffix = 'table.table_padrao td:not(.subtitle)';
        var selector = (selectorPrefix) ? selectorPrefix + ' ' + selectorSuffix : selectorSuffix;
        //This will be used to our stop condition
        var dateRegExp = new RegExp('[0-9]{2}/[0-9]{2}/[0-9]{4}');

        var returnData = {};
        var headers = [];
        var headerseInd = 0;

        //This way I can keep track of data retrieve, since this table has variable columns
        $(selectorPrefix + ' table.table_padrao th').each(function(index, tblTh){
            if(index > 0) {
                headers.push(tblTh.children[0].data.trim());
            }
        });

        $(selector).each(function (ind, tblTd) {
            //If I find a data, then we break line
            if(tblTd.children[0].data && tblTd.children[0].data.match(dateRegExp)) {
                values[headers[headerseInd]] = tblTd.children[0].data;
                objects.push(values);
                values = {};
                headerseInd = 0;//Keeping track of which data I'm getting
            } else {
                if (tblTd.children[0].name === 'a') {//special value from this table
                    values[headers[headerseInd]] = tblTd.children[0].children[0].data
                } else {
                    values[headers[headerseInd]] = tblTd.children[0].data;
                }
                headerseInd++;
            }
        });

        returnData.objects = objects;
        return returnData;
    };

    /**
     *
     * Search through the response for usable data. Statistic's data respectively.
     *
     * @param body {String} string with the web site's html
     * @returns {Array} Array of Arrays, which each item represents a line in the table
     */
    var dataFormatter1 = function(body) {
        var $ = cheerio.load(body);
        var values = [];
        var objects = [];
        var selector = 'table.table_padrao td:not(.subtitle)';

        $(selector).each(function (ind, tblTd) {
            if (tblTd.children[0].name === 'a') {
                if (values.length > 0) {
                    objects.push(values.slice());
                    values = [];
                }
                values.push(tblTd.children[0].children[0].data);
            } else {
                if(tblTd.children[0].data) values.push(tblTd.children[0].data);
            }
        });

        if(values.length > 0){
            objects.push(values);
        }
        return objects;
    };

    /**
     *
     * Request to the station's page.
     *
     * @param formData {Object} data to be sent to ANP web Site
     * @param fuel {Object} Fuel which we are searching for now
     * @param weekCode {String} code which ANP identifies a week
     * @param city {String} city's name
     * @param stateResponse {Object} unformatted data to be sent to requester
     * @param cities {Object} represents the cities to be sent to the requester, not formatted
     * @param cityBody {String} string which represents ANP web site's html
     * @param response {Object} express's response
     */
    var getStations = function(formData, fuel, weekCode, city, stateResponse, cities, cityBody, response) {
        var stationURL = config.anp.baseURL + config.anp.stationPath;
        var stationDAO = new StationDAO();
        var $ = cheerio.load(cityBody);

        //ANP API gets really messy, they have the same field with a different case. request was complaining about this
        delete formData.tipo;
        delete formData.txtMunicipio;
        formData.Tipo = 2;
        formData.selMunicipio = $('select[name=selMunicipio]').val();

        req.post({url: stationURL, form: formData, timeout: config.timeout}, function (error, resp, body) {
            if(!error){
                var stations = dataFormatter2(body, '#postos_nota_fiscal');

                stations.objects.forEach(function(stationValues){

                    var station = stationDAO.createStation(stationValues, fuel.name, city);

                    addStationToCities(station, cities);
                });

                sendRequest(response, cities, stateResponse);

            }else {
                console.log('Error');
                response.sendStatus(500);
            }
        });
    };

    /**
     *
     * Request to the statistic's page.
     *
     * @param formData {Object} data to be sent to ANP web Site
     * @param fuel {Object} Fuel which we are searching for now
     * @param stateResponse {Object} unformatted data to be sent to requester
     * @param cities {Object} represents the cities to be sent to the requester, not formatted
     * @param body {String} string which represents ANP web site's html
     * @param response {Object} express's response
     */
    var getStatistics = function(formData, fuel, stateResponse, cities, body, response) {
        var cityURL = config.anp.baseURL + config.anp.cityPath;
        var statistics = dataFormatter1(body);

        //Counting how many requests it remains till I send the data to the requester
        //This only works because of Node's EventHandler process (As a 'single' thread process)
        waiting += statistics.length;
        statistics.forEach(function(statisticValues){
            var statistic =  statisticsDAO.createStatistic(statisticValues);

            me.addStatisticsToCities(statistic, cities, fuel.name);

            //ANP API gets really messy, they have the same field with a different case. request was complaining about this
            formData.txtMunicipio = statistic.city;
            formData.selCombustivel = fuel.value;
            delete formData.Tipo;
            delete formData.txtMunicipio;
            formData.tipo = 1;

            req.post({url: cityURL, form: formData, timeout: config.timeout}, function (error, cityResponse, cityBody) {
                if(!error) {
                    getStations(formData, fuel, stateResponse.weekCode, statistic.city, stateResponse, cities, cityBody, response);
                } else {
                    var errorMsg = 'Could not crawl over web site ' + cityURL + '. ' + error;
                    console.log(errorMsg);
                    response.status(500).send(errorMsg);
                }
            });
        });

    };

    /**
     *
     * Nothing was found in the database, time to crawl over the ANO web site.
     *
     * @param response {Object} express's response
     * @param formData {Object} data to be sent to ANP web Site
     * @param weekCode {Number} number that ANP uses to identify a week
     * @param state {Object} unformatted data to be sent to requester
     */
    var getDataFromANPWebSite = function(response, formData, weekCode, state) {
        var valueURL = config.anp.baseURL + config.anp.valuesPath;
        var fuels = formData.fuels;
        var cities = {};

        console.log('Getting gas prices from ANP');
        fuels.forEach(function(fuel){
            formData['selCombustivel'] = fuel.value;
            req.post({url: valueURL, form: formData, timeout: config.timeout}, function (error, valuesResponse, valuesBody) {
                if (!error) {
                    getStatistics(formData, fuel, state, cities, valuesBody, response);
                } else {
                    var errorMsg = 'Could not crawl over web site ' + valueURL + '. ' + error;
                    console.log(errorMsg);
                    response.status(500).send(errorMsg);
                }
            });
        });
    };

    /**
     *
     * Decides if we get data from the database or to crawl over the site for information
     *
     * @param request {Object} express's resquest
     * @param response {Object} express's response
     */
    this.getGasPrice = function (request, response) {
        var formData = request.body;//created by body-parser
        var weekCode = formData['cod_Semana'];
        var statePriceValueDAO = new StatePriceValueDAO();
        var state = statePriceValueDAO.createStatePriceValues(formData.selEstado, weekCode);

        statePriceValueDAO.findStatePricesByWeekCode(ModelHelper.stringToNumber(weekCode), state.name, function(statePriceValues){
            console.log('Data found in Database');
            response.send(statePriceValues);
        }, function() {
            console.log('Could not retrieve data from database.');

            getDataFromANPWebSite(response, formData, weekCode, state);
        });
    };
};

module.exports = new gasPricesController();