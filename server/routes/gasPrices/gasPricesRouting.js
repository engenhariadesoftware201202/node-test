var gasPricesController = require('./gasPricesController');
var express = require('express');
var router = express.Router();

router.route('/')
    .post(gasPricesController.getGasPrice);

module.exports = router;