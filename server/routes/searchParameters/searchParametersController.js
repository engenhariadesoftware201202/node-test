var req = require('request');
var cheerio = require('cheerio');
var config = require('../../config');

var SearchParametersController = function () {
    var me = this;

    /**
     *
     * Make the request to retrieve which states ANP has available. Retrieves other important data so we can be
     * able to replicate all the call correctly.
     *
     * @param request {Object} express request
     * @param response {Object} express response
     */
    this.getParameters = function(request, response) {
        var url = config.anp.baseURL + config.anp.statesPath;
        console.log('Getting search parameters at ' + url);
        req({url: url, timeout: config.timeout}, function(error, resp, html) {
            if (!error) {
                var $ = cheerio.load(html);
                var result = {
                    states: [],
                    fuels: []
                };

                me.getSelectData($, 'selEstado', result.states);
                me.getSelectData($, 'selCombustivel', result.fuels);
                me.getInputData($, 'selSemana', result);
                me.getInputData($, 'desc_Semana', result);
                me.getInputData($, 'cod_Semana', result);
                me.getInputData($, 'tipo', result);

                console.log('Sending parameters data back to client');
                response.json(result);
            } else {
                var errorMsg = 'Could not crawl over ' + url + '. ' + error;
                console.log(errorMsg);
                response.status(500).send(errorMsg);
            }
        });
    };

    /**
     *
     * Uses cheerio to search through html and get the input's value
     *
     * @param $ {cheerio} html loaded by cheerio
     * @param selectName {String} input's name to be searched
     * @param store {Object} where we are gonna save the data retrieved
     */
    this.getInputData = function($, selectName, store) {
        store[selectName] = $('input[name='+ selectName + ']').get(0).attribs.value;
    };

    /**
     *
     * Uses cheerio to search through html and get the select's options
     *
     * @param $ {cheerio} html loaded by cheerio
     * @param selectName {String} select's name to be searched
     * @param store {Object} where we are gonna save the data retrieved
     */
    this.getSelectData = function($, selectName, store) {
        $('select[name=' + selectName + ']').find('option').each(function(index, item){
            var values = item.attribs.value;
            //Break down value so we have a nicer data to be displayed
             var firstFilter = values.split('*');
             var object = {
             	value: item.attribs.value,
             	name: firstFilter[1].replace(/@/g, ' ')
             };

            store.push(object);
        });
    }
};

module.exports = new SearchParametersController();