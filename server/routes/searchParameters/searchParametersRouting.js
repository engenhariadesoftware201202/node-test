var searchParametersController = require('./searchParametersController');
var express = require('express');
var router = express.Router();

router.route('/')
    .get(searchParametersController.getParameters);

module.exports = router;