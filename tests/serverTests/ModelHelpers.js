var should = require('should');
var ModelHelpers = require('../../server/helpers/modelHelpers');

describe('ModelHelpers', function(){
    it('should turn a string into number', function(){

        var stringNumber = '12.45';
        var number = ModelHelpers.stringToNumber(stringNumber);

        number.should.be.an.Number;
    });

    it('should remove * and @ from string', function() {
        var string = '123*hey@look';
        var formatted = ModelHelpers.normalizeString(string);

        formatted.should.be.a.String;
        formatted.should.matchAny(/^[^*@]+$/);

    });
})