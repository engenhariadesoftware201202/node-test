var should = require('should');
var searchParametersController = require('../../server/routes/gasPrices/gasPricesController');

describe('GasPricesController', function(){

    it('should format cities data', function(){
        var cities = {
            test: {
                statistics: [],
                stations: {
                    test2: 'u',
                    test3: 'v'
                }
            }
        };

        var newCity = searchParametersController.formatCitiesFromData(cities);

        newCity.should.be.an.Array;
        newCity[0].should.have.properties(['name']);
        newCity[0].statistics.should.have.lengthOf(0);
        newCity[0].stations.should.be.an.Array;
        newCity[0].stations.length.should.greaterThan(0);

    });
});