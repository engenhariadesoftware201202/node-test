var should = require('should');
var searchParametersController = require('../../server/routes/searchParameters/searchParametersController');
var cheerio = require('cheerio');

describe('SearchParametersController', function(){
    it('should get data from an input', function(){
        var store = {};
        var selectName = 'selName';
        var value = 'ahaha';
        var $ = cheerio.load('<html><header></header><body><input name=\"'+selectName+'\" value="'+value+'"></input></body></html>');

        searchParametersController.getInputData($, selectName, store);

        store[selectName].should.exist;
        store[selectName].should.be.equal(value);

    });

    it('should get data from a select', function(){
        var store = [];
        var selectName = 'selName';
        var value = 'ahaha';

        var $ = cheerio.load('<html><header></header><body><select name=\"'+selectName+'\" value=\"'+value+'\"><option value=\"12*asd\">oi</option></select></body></html>');

        searchParametersController.getSelectData($, selectName, store);

        store.length.should.greaterThan(0);

    });
});